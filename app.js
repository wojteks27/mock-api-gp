const express = require('express');
const app = express();
const API_PORT = process.env.API_PORT || 3030;

const handlers = require('./routes');

app.get('/api/users.json', handlers.json);
app.get('/api/users.xml', handlers.xml);
app.get('/api/users.csv', handlers.csv);

app.listen(API_PORT, () => console.log(`Example app listening on port ${API_PORT}!`))