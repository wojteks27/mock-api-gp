const jsonData = require('./json');
const csvData = require('./csv');
const xmlData = require('./xml');

module.exports = {
  json: jsonData,
  csv: csvData,
  xml: xmlData,
};