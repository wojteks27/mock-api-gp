const data = require('../data/data');
const CSVParser = require('json2csv').Parser;

const fields = ['first_name', 'last_name', 'email', 'gender', 'ip_address'];
const parser = new CSVParser({fields});

module.exports = (req, res) => {
  res.setHeader('Content-Type', 'text/csv');
  res.send(parser.parse(data));
};