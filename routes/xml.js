const data = require('../data/data');
const js2xmlparser = require('js2xmlparser');

module.exports = (req, res) => {
  res.setHeader('Content-Type', 'text/xml');
  res.send(js2xmlparser.parse("person", data));
};