const data = require('../data/data');

module.exports = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
}