# mock-api-gp   

## How to install?

* `npm i`
* `npm start`

## How to use?

If not configured differently (by explicitly setting environment variable `API_PORT`) the API will be exposed on port `3030`.

API exposes three endpoints:

* `/api/users.json`
* `/api/users.csv`
* `/api/users.xml`

GET parameters are ignored.